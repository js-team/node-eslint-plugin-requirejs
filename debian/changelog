node-eslint-plugin-requirejs (4.0.1-2) UNRELEASED; urgency=medium

  * Update standards version to 4.6.0, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 05 Feb 2022 07:21:32 -0000

node-eslint-plugin-requirejs (4.0.1-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Debian Janitor ]
  * set upstream metadata fields:
    Bug-Database Bug-Submit Repository Repository-Browse

  [ Jonas Smedegaard ]
  * update copyright info:
    + update coverage
    + use Reference field (not License-Reference);
      tighten lintian overrides
  * simplify source helper script copyright-check
  * update git-buildpackage config:
    + use DEP-14 branches debian/latest upstream/latest
    + add usage comment
  * use debhelper compatibility level 13 (not 12)
  * build-depend explicitly on node-eslint-config-eslint

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Sep 2021 19:08:08 +0200

node-eslint-plugin-requirejs (4.0.0-7) unstable; urgency=medium

  * stop depend on nodejs;
    have autopkgtest depend on nodejs
  * declare compliance with Debian Policy 4.5.1

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 27 Dec 2020 05:21:08 +0100

node-eslint-plugin-requirejs (4.0.0-6) unstable; urgency=medium

  * watch: drop unused repacksuffix
  * update helper script tap-todo:
    + pre-compile regexes
    + fix fail if no tests were run
    + fix counting, and emit error message on failure
  * build-depend on node-ignore, to improve test coverage
  * update patch 1001
  * update workarounds for failing tests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Apr 2020 02:13:38 +0200

node-eslint-plugin-requirejs (4.0.0-5) unstable; urgency=medium

  * stop tolerate stderr noise in autopkgtest
  * improve helper tool tap-todo to handle any extended test content
    (not only YAML data);
    simplify tests

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 27 Mar 2020 12:25:21 +0100

node-eslint-plugin-requirejs (4.0.0-4) unstable; urgency=medium

  * de-embed helper script tap-todo;
    fix build-depend on perl
  * copyright: extend coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 27 Mar 2020 11:41:10 +0100

node-eslint-plugin-requirejs (4.0.0-3) unstable; urgency=medium

  * use debhelper compatibility level 12 (not 10);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.0
  * add patch 1001 to lint code using eslint
  * check with eslint as build test;
    build-depend on eslint node-eslint-plugin-node
    (not node-eslint)
  * skip only specific failing mocha tests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 26 Mar 2020 22:47:18 +0100

node-eslint-plugin-requirejs (4.0.0-2) experimental; urgency=medium

  * fix clean generated docs

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 Nov 2019 10:59:00 +0100

node-eslint-plugin-requirejs (4.0.0-1) experimental; urgency=low

  * Initial release.
    Closes: Bug#943670.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 27 Oct 2019 19:25:13 +0100
